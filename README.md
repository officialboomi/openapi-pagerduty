# PagerDuty Connector
An incident response and operations platform.

Documentation: https://developer.pagerduty.com/api-reference/
Specification: https://github.com/PagerDuty/api-schema/blob/main/reference/REST/openapiv3.json

## Prerequisites

+ A PagerDuty account setup for either API Key or Auth2.0 authentication.

## Supported Operations

The following operations are not supported at this time:
* listEscalationPolicyAuditRecords
* listSchedulesAuditRecords
* listServiceAuditRecords
* listTeamsAuditRecords
* listUsersAuditRecords
* getServiceIntegration
* createServiceIntegration
* updateServiceIntegration

## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

